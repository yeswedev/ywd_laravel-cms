# YesWeDev - Laravel CMS

From now, Laravel CMS have severals branches to follow Laravel versions : 
- 0.x historical version, using laravel 5.6, 5.7, 5.8
- 6.x pour laravel 6.x
- 7.x pour laravel 7.x
- 8.x pour laravel 8.x 

## Installation


### Package installation

```
composer require yeswedev/laravel-cms
```

### Registering services with Laravel

In `config/app.php`, section "Package Service Providers":

```php
/*
 * Package Service Providers...
 */
YesWeDev\LaravelCMS\LaravelCMSServiceProvider::class,
```

### Setting up database

```
php artisan migrate
```
