<?php
namespace YesWeDev\LaravelCMS\Traits;

trait HasAttachments
{
    /**
     * Get the attachments that are assigned to this entity
     */
    public function attachments()
    {
        return $this->morphMany(
            'YesWeDev\LaravelCMS\Attachment',
            'owner'
        )->orderBy('slug', 'ASC');
    }

    /**
     * Get the related attachment using the given slug
     */
    public function getAttachmentBySlug(string $slug)
    {
        return $this->attachments()->where('slug', $slug)->first();
    }
}
