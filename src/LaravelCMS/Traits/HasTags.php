<?php
namespace YesWeDev\LaravelCMS\Traits;

trait HasTags
{
    /**
     * Get the tags that are assigned to this entity
     */
    public function tags()
    {
        return $this->morphToMany(
            'YesWeDev\LaravelCMS\Tag',
            'owner',
            'cms_tags_relationships',
            'owner_id',
            'tag_id'
        )->orderBy('slug', 'ASC');
    }
}
