<?php
namespace YesWeDev\LaravelCMS\Traits;

trait HasBlocks
{
    /**
     * Get the blocks that are assigned to this entity
     */
    public function blocks()
    {
        return $this->morphMany(
            'YesWeDev\LaravelCMS\Block',
            'owner'
        )->orderBy('priority', 'DESC');
    }

    /**
     * Get the related block using the given slug
     */
    public function getBlockBySlug(string $slug)
    {
        return $this->blocks()->where('slug', $slug)->first();
    }
}
