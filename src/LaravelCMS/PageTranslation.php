<?php

namespace YesWeDev\LaravelCMS;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $table = 'cms_page_translations';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'location',
        'url',
        'seo_title',
        'seo_h1',
        'seo_description',
    ];

    /**
     * Set the url's resource.
     *
     * @param  string  $value
     * @return void
     */
    public function setUrlAttribute($value)
    {
        setlocale(LC_ALL, 'fr_FR.UTF-8');
        $this->attributes['url'] = preg_replace('/[\s\']+/', '-', strtolower(iconv("UTF-8", "ASCII//TRANSLIT", $value)));
    }
}
