<?php

namespace YesWeDev\LaravelCMS;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use Translatable;
    use SoftDeletes;

    protected $table = 'cms_tags';

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $fillable = [
        'slug',
    ];

    protected $with = [
        'translations'
    ];

    /**
     * Get the objects that are assigned this tag
     */
    public function owners(string $class)
    {
        return $this->morphedByMany(
            $class,
            'owner',
            'cms_tags_relationships',
            'tag_id',
            'owner_id'
        )->orderBy('priority', 'DESC');
    }
    public function archives()
    {
        return $this->owners('YesWeDev\LaravelCMS\Archive');
    }
    public function posts()
    {
        return $this->owners('YesWeDev\LaravelCMS\Post');
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (! $this->slug) {
            $count = self::count();
            $this->slug = 'tag-'.uniqid();
        }
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($this->fireModelEvent('saving') === false) {
                    return false;
                }

                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            return $this->saveTranslations();
        }

        return false;
    }
}
