<?php

namespace YesWeDev\LaravelCMS;

use Illuminate\Database\Eloquent\Model;

class BlockTranslation extends Model
{
    protected $table = 'cms_block_translations';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
    ];
}
