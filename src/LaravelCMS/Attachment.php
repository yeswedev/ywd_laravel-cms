<?php

namespace YesWeDev\LaravelCMS;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use Translatable;

    protected $table = 'cms_attachments';

    public $translatedAttributes = [
        'title',
        'description',
        'src',
    ];

    protected $fillable = [
        'type',
    ];

    protected $with = [
        'translations'
    ];

    /**
     * Get the object that is assigned this attachment
     */
    public function owner()
    {
        return $this->morphTo();
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $this->slug = $this->type.'_'.$this->owner->slug.uniqid();
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($this->fireModelEvent('saving') === false) {
                    return false;
                }

                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            return $this->saveTranslations();
        }

        return false;
    }
}
