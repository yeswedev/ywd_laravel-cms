<?php

namespace YesWeDev\LaravelCMS;

use Illuminate\Database\Eloquent\Model;

class AttachmentTranslation extends Model
{
    protected $table = 'cms_attachment_translations';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'src',
    ];
}
