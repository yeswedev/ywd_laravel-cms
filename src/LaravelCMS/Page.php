<?php

namespace YesWeDev\LaravelCMS;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

use YesWeDev\LaravelCMS\Traits\HasAttachments;
use YesWeDev\LaravelCMS\Traits\HasBlocks;
use YesWeDev\LaravelCMS\Traits\HasTags;

class Page extends Model
{
    use Translatable;
    use SoftDeletes;
    use HasAttachments;
    use HasBlocks;
    use HasTags;

    protected $table = 'cms_pages';
    protected $translationForeignKey = 'page_id';
    protected $translationModel = 'YesWeDev\LaravelCMS\PageTranslation';

    protected $translatedAttributes = [
        'title',
        'description',
        'location',
        'url',
        'seo_title',
        'seo_h1',
        'seo_description',
    ];

    protected $dates = [
        'publication',
    ];

    protected $fillable = [
        'publication',
        'reference',
        'slug',
        'type',
    ];

    protected $with = [
        'translations'
    ];

    /**
     * Get the page that owns the current page.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function parent()
    {
        return $this->belongsTo(self::class);
    }

    /**
     * Get the pages owning the current page
     * /!\ Obsolete method, only used to ease upgrade from 0.26 and older
     */
    public function parents()
    {
        if (Schema::hasTable('cms_pages_relationships')) {
            return $this->belongsToMany(
                'YesWeDev\LaravelCMS\Page',
                'cms_pages_relationships',
                'child_id',
                'parent_id'
            )->orderBy('priority', 'DESC');
        } else {
            throw new \Exception(self::class.'::parents is no longer available, use '.self::class.'::parent instead.');
        }
    }

    /**
     * Get the parent using the given slug
     * /!\ Obsolete method, only used to ease upgrade from 0.26 and older
     */
    public function getParentBySlug(string $slug)
    {
        $parent = $this->parent;
        if ($parent != null && $parent->slug == $slug) {
            return $parent;
        } else {
            return null;
        }
    }

    /**
     * Check if a given archive owns this page
     */
    public function isOwnedByPage(int $id)
    {
        $parent = $this->parent;
        return ($parent != null && $parent->id == id);
    }

    /**
     * Get the pages owned by the current page
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function children()
    {
        if (Schema::hasTable('cms_pages_relationships')) {
            // /!\ Obsolete relationship, only used to ease upgrade from 0.26 and older
            return $this->belongsToMany(
                'YesWeDev\LaravelCMS\Page',
                'cms_pages_relationships',
                'parent_id',
                'child_id'
            )->orderBy('priority', 'DESC');
        } else {
            return $this->hasMany(self::class, 'parent_id')
                ->orderBy('priority', 'DESC');
        }
    }

    /**
     * Get the child using the given slug
     */
    public function getChildBySlug(string $slug)
    {
        return $this->children()->where('slug', $slug)->first();
    }

    /**
     * Check if a given page is owned by this page
     */
    public function ownsPage(int $id)
    {
        $child = $this->children()->where('id', $id)->first();
        return ($child !== null);
    }
}
