<?php

namespace YesWeDev\LaravelCMS;

class Archive extends Page
{
    public function __construct(array $attributes = [])
    {
        $attributes['type'] = 'archive';
        parent::__construct($attributes);
    }

    // Allow to use static methods 'all', 'find', 'count' and 'where' on this class
    public static function all($columns = ['*'])
    {
        return parent::where('type', 'archive')
            ->orderBy('priority', 'DESC')
            ->get($columns);
    }
    public static function find($id, $columns = ['*'])
    {
        return parent::where('type', 'archive')->find($id, $columns);
    }
    public static function count($columns = '*')
    {
        return parent::where('type', 'archive')->count($columns);
    }
    public function __call($method, $parameters)
    {
        if (in_array($method, ['increment', 'decrement'])) {
            return $this->$method(...$parameters);
        }
        if (in_array($method, ['where'])) {
            return $this->newQuery()
                ->where('type', 'archive')
                ->orderBy('priority', 'DESC')
                ->$method(...$parameters);
        }
        return $this->newQuery()->$method(...$parameters);
    }
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (! $this->slug) {
            $count = self::count();
            $this->slug = 'archive-'.uniqid();
        }
        if (! $this->priority) {
            $this->priority = 1;
        }
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($this->fireModelEvent('saving') === false) {
                    return false;
                }

                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            return $this->saveTranslations();
        }

        return false;
    }
}
