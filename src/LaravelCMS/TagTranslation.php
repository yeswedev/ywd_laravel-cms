<?php

namespace YesWeDev\LaravelCMS;

use Illuminate\Database\Eloquent\Model;

class TagTranslation extends Model
{
    protected $table = 'cms_tag_translations';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'description'
    ];
}
