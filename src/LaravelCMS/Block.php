<?php

namespace YesWeDev\LaravelCMS;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

use YesWeDev\LaravelCMS\Traits\HasAttachments;

class Block extends Model
{
    use HasAttachments;
    use Translatable;

    protected $table = 'cms_blocks';

    public $translatedAttributes = [
        'title',
        'description',
    ];

    protected $fillable = [
        'layout',
        'slug'
    ];

    protected $with = [
        'translations'
    ];

    /**
     * Get the page that is assigned this block
     */
    public function owner()
    {
        return $this->morphTo();
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (! $this->slug) {
            $this->slug = 'block-'.uniqid();
        }
        if (! $this->title) {
            $this->title = $this->owner->title.' — '.ucfirst(preg_replace('/[-_]/', ' ', $this->slug));
        }
        if (! $this->priority) {
            $this->priority = 1;
        }
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($this->fireModelEvent('saving') === false) {
                    return false;
                }

                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        } elseif (parent::save($options)) {
            // We save the translations only if the instance is saved in the database.
            return $this->saveTranslations();
        }

        return false;
    }
}
