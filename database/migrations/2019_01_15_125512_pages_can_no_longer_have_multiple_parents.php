<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use YesWeDev\LaravelCMS\Page;

class PagesCanNoLongerHaveMultipleParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_pages', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')->on('cms_pages')
                ->onDelete('set null');
        });
        // Set parent on existing pages
        $pages = Page::all();
        foreach ($pages as $page) {
            $parent = $page->parents()->first();
            if ($parent != null) {
                $page->parent_id = $parent->id;
                $page->save();
            }
        }
        Schema::dropIfExists('cms_pages_relationships');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('cms_pages_relationships', function (Blueprint $table) {
            $table->unsignedInteger('child_id');
            $table->foreign('child_id')
                ->references('id')->on('cms_pages')
                ->onDelete('cascade');
            $table->unsignedInteger('parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('cms_pages')
                ->onDelete('cascade');
        });
        // Restore parents on existing pages
        $pages = Page::all();
        foreach ($pages as $page) {
            $parent = Page::find($page->parent_id);
            if ($parent != null) {
                $page->parents()->attach($page->parent_id);
            }
        }
        Schema::table('cms_pages', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
            $table->dropColumn('parent_id');
        });
    }
}
