<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachablesRelationshipsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_tags_relationships', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->foreign('tag_id')
                ->references('id')->on('cms_tags')
                ->onDelete('cascade');
            $table->unsignedInteger('owner_id');
            $table->string('owner_type');
            $table->unique([ 'tag_id', 'owner_id', 'owner_type' ], 'cms_tags_relationships_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_tags_relationships');
    }
}
