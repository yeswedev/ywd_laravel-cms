<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueKeyOnAttachement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_attachments', function (Blueprint $table) {
            $table->dropUnique('cms_attachments_type_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_attachments', function (Blueprint $table) {
            $table->unique([ 'owner_id', 'owner_type', 'type' ], 'cms_attachments_type_unique');
        });
    }
}
