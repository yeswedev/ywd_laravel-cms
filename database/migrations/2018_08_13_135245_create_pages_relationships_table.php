<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pages_relationships', function (Blueprint $table) {
            $table->unsignedInteger('child_id');
            $table->foreign('child_id')
                ->references('id')->on('cms_pages')
                ->onDelete('cascade');
            $table->unsignedInteger('parent_id');
            $table->foreign('parent_id')
                ->references('id')->on('cms_pages')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_pages_relationships');
    }
}
