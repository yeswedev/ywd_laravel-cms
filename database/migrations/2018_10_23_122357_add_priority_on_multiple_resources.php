<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriorityOnMultipleResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ([ 'cms_pages', 'cms_blocks' ] as $name) {
            Schema::table($name, function (Blueprint $table) {
                $table->float('priority')
                    ->default(1.0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ([ 'cms_pages', 'cms_blocks' ] as $name) {
            Schema::table($name, function (Blueprint $table) {
                $table->dropColumn('priority');
            });
        }
    }
}
