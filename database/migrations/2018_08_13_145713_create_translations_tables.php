<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page_translations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('location')->nullable();
            $table->string('locale')
                ->index()
                ->default('fr');
            $table->unsignedInteger('page_id');
            $table->foreign('page_id')
                ->references('id')->on('cms_pages')
                ->onDelete('cascade');
            $table->unique([ 'page_id', 'locale' ]);
        });
        foreach ([ 'attachment', 'block', 'tag' ] as $entity) {
            $entities = $entity.'s';
            Schema::create('cms_'.$entity.'_translations', function (Blueprint $table) use ($entity, $entities) {
                $table->increments('id')->unsigned();
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->string('locale')->index()
                    ->default('fr');
                $table->unsignedInteger($entity.'_id');
                $table->foreign($entity.'_id')
                    ->references('id')->on('cms_'.$entities)
                    ->onDelete('cascade');
                $table->unique([ $entity.'_id', 'locale' ]);
            });
        }
        Schema::table('cms_attachment_translations', function (Blueprint $table) {
            $table->string('src')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ([ 'attachment', 'block', 'tag' ] as $entity) {
            Schema::dropIfExists('cms_'.$entity.'_translations');
        }
        Schema::dropIfExists('cms_page_translations');
    }
}
